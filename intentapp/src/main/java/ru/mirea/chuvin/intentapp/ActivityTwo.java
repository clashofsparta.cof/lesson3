package ru.mirea.chuvin.intentapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ActivityTwo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        TextView tw = findViewById(R.id.textView);
        Intent it = getIntent();
        String messageFromAcOne = it.getStringExtra("acOne");
        tw.setText("КВАДРАТ ЗНАЧЕНИЯ\n" +
                "МОЕГО НОМЕРА ПО СПИСКУ В ГРУППЕ СОСТАВЛЯЕТ 900, а текущее\n" +
                "время " + messageFromAcOne);
    }
}